terraform {
  cloud {
      organization = "Swisscom-Schweiz-AG"
 
      workspaces {
        name = "my-amazing-project"
      }
  }

  required_providers {
    tos = {
      source  = "registry.terraform.io/SCS-Tufin/tos"
      version = ">= 1.0.26"
    }

  }
  required_version = ">= 0.14.9"
}


provider "tos" {
  tba_url    = "https://tufinapps-alb-1503945167.eu-central-1.elb.amazonaws.com"

  mode = "tba"
}
